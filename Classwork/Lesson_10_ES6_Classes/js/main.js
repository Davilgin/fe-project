
// class Product {
//     constructor(id, name, price) {
//         this._id = id;
//         this.name = name;
//         this.price = price;
//     }
//
//     get id() {
//         return this._id;
//     }
//
//
//     makeDiscount(discount) {
//         this.price -= discount;
//         return this.price;
//     }
//
//     print() {
//         console.log(`| ID: ${this.id} || NAME: ${this.name}  || PRICE: ${this.price } |`)
//     }
//
//     static compare(a, b) {
//         return a.price - b.price;
//     }
//
//     // static compare(a, b) {
//     //     if (a.price > b.price) {
//     //         return -1;
//     //     } else if (a.price < b.price) {
//     //         return 1;
//     //     } else {
//     //         return 0;
//     //     }
//     // }
// }
//
// const orange = new Product(1, 'Orange', 15);
// const banana = new Product(2, 'Banana', 13);
// const apple = new Product(3, 'Apple', 16);
//
// // orange.makeDiscount(3);
// // console.log(Product.compare(orange, banana));
// // console.log(orange.print());
// // console.log(banana.print());
// // console.log(apple.print());
// // const list = [new Product(1, 'Tomato', 100),new Product(2, 'Apple', 50),new Product(3, 'Orange', 30)];
// // list.sort(Product.compare);
// // console.log(list);
//
// class Book extends Product {
//     constructor(...args) {
//         super(...args);
//         const [,,, authors, date] = args;
//         this.authors = authors;
//         this.date = date;
//     }
//     print() {
//         super.print();
//         console.log(`| Authors ${this.authors} || publication Date ${this.date} |`);
//     }
// }
//
// const holidays = new Book( 3,'Vergili', 234,['Abraham', 'Stephan'], (new Date().toLocaleString()) );
// holidays.print();
// console.log(holidays);

// ======== Task 2 ==================

class Walker {

    constructor(parentEl, step = 10) {
        this.element = document.createElement('div');
        this.element.className = 'human';
        this.step = step;

        parentEl.appendChild(this.element);
    }

    walk() {
        const isAlreadyMove = !!this.element.style.transform;
        if (!isAlreadyMove) {
            this.element.style.transform = `translateX(${this.step}px)`;
        } else {
            let prevState = this.element.style.transform;
            prevState = +prevState.substring(11).slice(0, -3);
            this.element.style.transform = `translateX(${prevState + this.step}px)`;
        }

        return this
    }

}
// const humanBlock = document.querySelector('.human');

const human = new Walker(document.body, 15);
setTimeout(() => human.walk(), 1000);

human.walk().walk().walk();