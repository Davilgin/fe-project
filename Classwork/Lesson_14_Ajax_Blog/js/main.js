
const createFormShowBtn = document.querySelector('.show-create-form-btn');

createFormShowBtn.addEventListener('click', showCreateForm);

function showFormOnPage(form) {
    document.body.appendChild(form);
}

async function showCreateForm() {
    const fields = await getCreateFormField();
    const form = buildCreateForm(fields, onCreateFormSubmit);
    console.log('done');
    showCreateForm(form);
}



async function getCreateFormField() {
    const res = await fetch('http://localhost:3010/create-form');
    const fields = await res.json();
    return fields;
}

function buildCreateForm(fields) {
    const form = document.createElement('form');
    form.classList.add('create-form');
    for (let {type, name} of fields) {
        const field = document.createElement('input');
        field.type = type;
        field.name = name;
        form.appendChild(field);
    }
    const submitBtn = document.createElement('button');
    submitBtn.type = 'submit';
    form.appendChild(submitBtn);

    // form.addEventListener('submit', onCreateFormSubmit); Можна і так викликати форму
    form.addEventListener('submit', onSubmit);

    return form;
}

function onCreateFormSubmit(e) {
    e.preventDefault();
    const values = {};
    for (let el of this.elements) {
        if ('value' in el) values[el.name] = el.value;
    }
}