// ============= Closures ========================

// =========== Task 1 ================================
//
// function createCounter() {
//     let value = 0;
//
//     return {
//         getValue() {
//             return value;
//         },
//         increaseValue() {
//             return ++value;
//         }
//     }
// }
//
// const counter = createCounter();
//
// console.log(counter.getValue());
// console.log(counter.increaseValue());
// console.log(counter.increaseValue());
// console.log(counter.increaseValue());
// console.log(counter.getValue());

// ========== Task 2 ===============================
//
// function createGrownList() {
//     let list = [];
//     return {
//         getList() {
//             return Array.of(...new Set([...list]))
//             // [...list]
//         },
//         addValues(number) {
//             if (!list.length || Math.max(...list) < number) {
//                 return list.push(number);
//             } else {
//                 return console.log(`This value ${number} is lower than last`);
//             }
//         }
//     }
// }
//
// const counter = createGrownList();
// counter.addValues(1);
// counter.addValues(2);
// counter.addValues(3);
// counter.addValues(1);
//
// const createdList = counter.getList();
// createdList.push(2);
// // console.log(counter.addValues(34));
//
// counter.getList();
// console.log(createdList);
// console.log(counter.getList());

