// (function () {
//     sum();
//
//     function  sum(a, b) {
// return a + b;
//     }
// })();

// ========= TryCatch ===========================================================

// function returnError() {
//      throw new Error('Error form function')
// }

// try {
//     const obj = {};
//     // obj.d.d
//     const userInput = prompt('Say age');
//     if(isNaN(userInput)) {
//         throw new Error('Age is not valid');
//     }
// } catch (error) {
//     console.log(error)
// }

// function doSomething() {
//     try {
//         const pseudoJSON = [];
//         const data = JSON.parse(pseudoJSON);
//     } catch (error) {
//         if(error.message === "Unexpected end of JSON input") {
//             alert('Sorry, data invalid. Try later');
//
//         }
//         console.log(error.message)
//     } finally {
//         console.log('Finally')
//     }
// }
// doSomething();
// console.log('We are here');

// // ========== Task 1 ========================
//
// function userData() {
//     try {
//         const name = prompt('Enter your name:');
//         if (name === null || name.split(' ').length !== 1 ) {
//             throw new Error('Wrong name!')
//         } else {
//             console.log(`Hello, ${name}`)
//         }
//
//         const age = prompt('Enter your age:');
//         if (isNaN(age) || age === null) {
//             throw new Error('Wrong age!')
//         } else {
//             console.log(`your age is ${age}`)
//         }
//     } catch (error) {
//         console.log(error)
//     }
// }
//
// userData();
//
// // console.log(userData);

function Warrior(name) {
    this.name = name;
    this.health = 100;
    this.isAlive = true;
    this.power = 100;
}

Warrior.prototype.heal = function (points = 100) {
    if (!isNaN(points)) {
        this.health += points;
        if (this.health > 100) {
            this.health = 100;
        }
        return this.health
    }
};

Warrior.prototype.takeDamage = function (damage = 0) {
    if (!isNaN(damage)) {
        this.health -= damage;
        this.health = Math.max(0, this.health);
        // if (this.health < 0) {
        //     return this.health = 0
        // }
        if (this.health === 0) {
            this.isAlive = false;
        }
    }
    return this.health
};

// Warrior.prototype.isAlive = function (alive = true) {
//     if (this.health === 0) {
//         this.alive = false;
//         return this.alive
//     } else {
//         this.alive = true;
//         return this.alive;
//     }
// };

Warrior.prototype.attack = function () {
    return randomInteger(0, this.power)
};

Warrior.prototype.defence = function (damage) {
    if (!isNaN(damage)) {
        let chance = randomInteger(0, 1);
        if (chance) {
            return this.takeDamage(damage);
        } else {
            return console.log(`${this.name} resist this attack`)
        }
    }
    return this.health
};

const warriorOne = new Warrior('Aladin');
const warriorTwo = new Warrior('Konnan');

function Arena(warriorOne, warriorTwo) {
    this.warriorOne = warriorOne;
    this.warriorTwo = warriorTwo;
    this.isFight = false;
}

Arena.prototype.start = function () {
    if (!this.isFight) {
        this.isFight = true
    } else {
        console.log('Fight is already running')
    }
};

Arena.prototype.nextRound = function () {
    if (this.isFight) {
        let warOneDamage = warriorOne.attack();
        console.log(`${warriorOne.name} deal ${warOneDamage} damage`);
        warriorTwo.defence(warOneDamage);
        console.log(warriorTwo);
        if (warriorTwo.isAlive === false && warriorOne.isAlive === false) {
            console.log('Warriors killed each other');
            battleArena.isFight = false;
        } else if (warriorTwo.isAlive === false) {
            console.log(`${warriorOne.name} is win this battle by defeating ${warriorTwo.name}`);
            battleArena.isFight = false;
        }
        let warTwoDamage = warriorTwo.attack();
        console.log(`${warriorTwo.name} deal ${warTwoDamage} damage`);
        warriorOne.defence(warTwoDamage);
        console.log(warriorOne);
        if (warriorTwo.isAlive === false && warriorOne.isAlive === false) {
            console.log('Warriors killed each other');
            battleArena.isFight = false;
        } else if (warriorOne.isAlive === false) {
            console.log(`${warriorTwo.name} is win this battle by defeating ${warriorOne.name}`);
            battleArena.isFight = false;
        }
    } else {
        console.log('Fight is not started')
    }
};

const battleArena = new Arena(warriorOne, warriorTwo);
battleArena.start();
battleArena.nextRound();


//
// warriorOne.heal();
// warriorOne.takeDamage();
// warriorOne.takeDamage(45);
// warriorOne.heal(10);
// warriorOne.heal();
// warriorOne.takeDamage(45);
//
// warriorOne.defence(25);
// warriorOne.defence(25);
// warriorOne.defence(25);
// warriorOne.defence(25);
// warriorOne.defence(25);
//
// console.log(warriorOne);
// console.log(warriorTwo);

function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}