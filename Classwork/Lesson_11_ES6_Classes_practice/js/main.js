// class Product {
//     constructor(id, name, price) {
//         this._id = id;
//         this.name = name;
//         this.price = price;
//     }
//
//     get id() {
//         return this._id;
//     }
//
//
//     makeDiscount(discount) {
//         this.price -= discount;
//         return this.price;
//     }
//
//     print() {
//         console.log(`| ID: ${this.id} || NAME: ${this.name}  || PRICE: ${this.price } |`)
//     }
//
//     static compare(a, b) {
//         return a.price - b.price;
//     }
//
//     // static compare(a, b) {
//     //     if (a.price > b.price) {
//     //         return -1;
//     //     } else if (a.price < b.price) {
//     //         return 1;
//     //     } else {
//     //         return 0;
//     //     }
//     // }
// }
//
// const orange = new Product(1, 'Orange', 15);
// const banana = new Product(2, 'Banana', 13);
// const apple = new Product(3, 'Apple', 16);
//
// // orange.makeDiscount(3);
// // console.log(Product.compare(orange, banana));
// // console.log(orange.print());
// // console.log(banana.print());
// // console.log(apple.print());
// // const list = [new Product(1, 'Tomato', 100),new Product(2, 'Apple', 50),new Product(3, 'Orange', 30)];
// // list.sort(Product.compare);
// // console.log(list);
//
// class Book extends Product {
//     constructor(...args) {
//         super(...args);
//         const [,,, authors, date] = args;
//         this.authors = authors;
//         this.date = date;
//     }
//     print() {
//         super.print();
//         console.log(`| Authors ${this.authors} || publication Date ${this.date} |`);
//     }
// }
//
// const holidays = new Book( 3,'Vergili', 234,['Abraham', 'Stephan'], (new Date().toLocaleString()) );
// holidays.print();
// console.log(holidays);

// // ======== Task 2 ==================
//
// class Walker {
//
//     constructor(parentEl, step = 10) {
//         this.element = document.createElement('div');
//         this.element.className = 'human';
//         this.step = step;
//
//         parentEl.appendChild(this.element);
//     }
//
//     walk() {
//         const isAlreadyMove = !!this.element.style.transform;
//         if (!isAlreadyMove) {
//             this.element.style.transform = `translateX(${this.step}px)`;
//         } else {
//             let prevState = this.element.style.transform;
//             prevState = +prevState.substring(11).slice(0, -3);
//             this.element.style.transform = `translateX(${prevState + this.step}px)`;
//         }
//
//         return this
//     }
//
// }
// // const humanBlock = document.querySelector('.human');
//
// const human = new Walker(document.body, 15);
// setTimeout(() => human.walk(), 1000);
//
// human.walk().walk().walk();

// class Walker {
//     constructor(parentEL, step = 10) {
//         this.element = document.createElement('div');
//         this.element.className = 'walker';
//         this.step = step;
//         this.distance = 0;
//
//         parentEL.appendChild(this.element)
//     }
//
//     walk() {
//         // const isAlreadyMoved = !!this.element.style.transform;
//         // if (!isAlreadyMoved) {
//         //     this.element.style.transform = `translateX(${this.step}px)`
//         // } else {
//         //     let prevState = this.element.style.transform;
//         //     prevState = +prevState.substring(11).slice(0, -3);
//         //     this.element.style.transform = `translateX(${prevState + this.step}px)`
//         // }
//         this.moveX(this.step);
//
//         return this;
//     }
//
//     moveX(distance) {
//         this.distance += distance;
//         this.element.style.transform = `translateX(${this.distance}px)`;
//     }
// }
//
// // const walker = new Walker(document.body, 30);
// // const walker2 = new Walker(document.body, 30);
// // setTimeout(() => walker.walk(), 1000);
// // walker.walk()
//
// class Runner extends Walker {
//     constructor(parentEl, step) {
//         super(parentEl, step);
//         this.stamina = 100;
//
//     }
//
//     walk() {
//         return this.moveX(this.step, 10);
//     }
//
//     run() {
//         return this.moveX(this.step * 3, 30);
//     }
//
//     moveX(distance, staminaCost) {
//         if (this.stamina >= staminaCost) {
//             super.moveX(distance);
//             this.stamina -= staminaCost;
//         } else {
//             return alert("Sorry, Im too tired");
//         }
//         return this;
//     }
// }
//
// const runner = new Runner(document.body, 30);
// // console.log(runner.walk());
// console.log(runner);
//
// // runner.walk().run().walk().run().walk().walk();
// // console.log(runner);

class Machine {
    constructor(power = 0) {
        this.isEnabled = false;
        this.power = power;
    }

    turnOn() {
        if (this.isEnabled === false) {
            return this.isEnabled = true;
        } else {
            alert('Machine is already turned on!');
        }
    }

    turnOff() {
        if (this.isEnabled === true) {
            return this.isEnabled = false;
        } else {
            alert('Machine is already turned off!');
        }
    }
}

class PureCoffeeMachine extends Machine {
    constructor(power, milkCapacity = 0, coffeeCapacity = 0) {
        super(power);
        this.milkCapacity = milkCapacity;
        this.coffeeCapacity = coffeeCapacity;
        // this.inProgress = false;
        this.processTimer = null;
    }

    // makeCoffee() {
    //     if (this.isEnabled === false) {
    //         console.log('Sorry coffee Machine is Off, turn it On');
    //     } else if (this.inProgress === true) {
    //         alert('Sorry, machine is busy now')
    //     } else if (this.milkCapacity < 1 || this.coffeeCapacity < 1) {
    //         console.log('Coffee Machine is not enough coffee or milk')
    //     } else {
    //         this.inProgress = true;
    //         this.milkCapacity -= 1;
    //         this.coffeeCapacity -= 1;
    //         setTimeout(() => {
    //             if (this.inProgress === false) {
    //                 return alert('Something went wrong')
    //             } else {
    //                 console.log('Your coffee is ready');
    //                 this.inProgress = false
    //             }
    //         }, 3000);
    //     }
    //
    //     return this;
    // }

    makeCoffee() {
        if (!this.isEnabled) return console.log('Sorry coffee Machine is Off, turn it On');
        if (this.processTimer) return console.log('Sorry, machine is busy now');
        if (!this.milkCapacity || !this.coffeeCapacity) return console.log('Coffee Machine is not enough coffee or milk');
        this.milkCapacity--;
        this.coffeeCapacity--;
        this.processTimer = setTimeout(() => {
                console.log('Your coffee is ready');
                this.processTimer = null;
        }, 3000);
    }

    checkForAvailability() {
        if (!this.isEnabled) return console.log("Turn On machine first");
        if (this.processTimer) return console.log("Sorry, machine is busy now");
        if (!this.coffeeCapacity || !this.milkCapacity) return console.log("Sorry, resources are out");
        return true;
    }


    turnOff() {
        super.turnOff();
        clearTimeout(this.processTimer);
        this.processTimer = null;
    }
}

// const galka = new PureCoffeeMachine(true, 10, 10);

// galka.turnOn();
// galka.makeCoffee();

const coffees = [
    {
        name: "Late",
        coffee: 1,
        milk: 2
    },
    {
        name: "Espresso",
        coffee: 3,
        milk: 0
    },
    {
        name: "Americano",
        coffee: 2,
        milk: 0
    },
    {
        name: "MilkedAmericano",
        coffee: 2,
        milk: 1
    },
];

class MediumCoffeeMachine extends PureCoffeeMachine {
    constructor(power, milkCapacity, coffeeCapacity, coffeeList = []) {
        super(power, milkCapacity, coffeeCapacity);
        this.coffeeList = coffeeList;

    }
    // getList() {
    //     this.coffeeList.forEach( (obj) => {
    //         console.log(obj);
    //         console.log(obj.index);
    //         console.log(obj.index + 1 + `${this.name}`)
    //     });
        // this.coffeeList.forEach( (obj) => {
        //     const {name, coffee, milk} = obj;
        //     console.log(`name: ${this.name}, coffee: ${this.coffee}, milk: ${this.milk}`)
        //
        // })
    // }

    getList() {
        let list = this.coffeeList.reduce((acc, {
            name
        }, index) => acc += `${index + 1}.${name}\n`, '');
        console.log(list)
    }

    makeCoffee(coffeeType) {
        if (this.checkForAvailability()) {
            let coffeeData;
            if (typeof coffeeType === 'number') {
                coffeeData = this.coffeeList[coffeeType - 1];
            }
            if (typeof coffeeType === 'string') {
                coffeeData = this.coffeeList.find(({name}) => name === coffeeType)
            }
            const {
                name,
                coffee,
                milk
            } = coffeeData;
            this.coffeeCapacity -= coffee;
            this.milkCapacity -= milk;
            this.processTimer = setTimeout(() => {
                console.log(`Your ${name} is ready.`);
                this.processTimer = null
            }, 3000)
        }
    }

}

const newList = new MediumCoffeeMachine(100,100,100,  coffees);

console.log(newList);
newList.getList();