// =============================== Additional ==========================================

class Animal {
    constructor() {
        this.health = 100;
        this.isFlight = true;
    }
    heal(points) {
        this.health += points;
    }


}

const animalOne = new Animal();

console.log(animalOne);

class Rabbit extends Animal {

    constructor(name) {
        super();
        this.name = name;
    }

    scream(points) {
        alert('AAAAAAAaaaaaaaAaaaAAA')
    }

    heal(points) {
        super.heal(points);
        alert('Thanks for that, AAAaaaaaaaaaAAAAAAAAA.');
    }
}

Animal.prototype.sayHi = () => alert('Hi im bird');

const rabbit = new Rabbit('Jack');

console.log(rabbit);
rabbit.heal(100);
console.log(rabbit);