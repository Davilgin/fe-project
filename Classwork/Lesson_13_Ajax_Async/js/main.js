const form = document.querySelector('form');
const list = document.querySelector('#notes');
console.log(list);
const notesURL = 'http://localhost:3000/notes';
const deleteBtn = document.getElementsByClassName('.delete-btn');

form.addEventListener('submit', function (e) {
    e.preventDefault();
    const title = this.elements.title.value;
    const text = this.elements.text.value;

    if (title.trim() && text.trim()) {
        fetch(notesURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title,
                text
            })
        }).then(res => {
            if (res.status === 201) {
                return res.json()
            } else {
                return res.text()
            }
        }).then(data => {
            if (data.id) {
                list.appendChild(createListItemDOM(data));
            } else {
                console.log(data)
            }
        })

    } else {
        alert('Fill all fields!')
    }
});

async function getNotes() {
    const res = await fetch(notesURL);
    const notes = await res.json();
    console.log(notes);

    notes.forEach(element => {
        list.appendChild(createListItemDOM(element))
    })
}

getNotes();

function createListItemDOM({title, text}) {
    const li = document.createElement('li');
    li.innerHTML = `
        <h5>${title}</h5>
        <p>${text}</p>
        <button data-delete="${id}" >delete</button>
    `;
    return li;
}

list.addEventListener('click', function (e) {
    // if (e.target.classList.contains('delete-btn')) {
    if (e.target.dataset.delete) {
        // console.log(this.e.target);
        const isDeleted = deleteNoteFromServer(e.target.dataset.delete);
        if (isDeleted) e.target.closest('li').remove();

    }
});

async function deleteNoteFromServer(id) {
    const res = await fetch(`${notesURL}/${id}`, {
        method: 'DELETE'
    });
    const answer = await res.json();
    if (!answer.length) {
        return true;
    }
}

