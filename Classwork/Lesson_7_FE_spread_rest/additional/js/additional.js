
// const user = {
//     name: 'John',
//     age: 30,
//     isAdmin: false
// };

// function User(name, age, isAdmin = false) {
//  return {
//      name,
//      age,
//      isAdmin
//  }
// }
//
// const user = User();

function Counter(step) {
    this.value = 0;
    this.step = step;
}

Counter.prototype.increment = function () {
    this.value += this.step;
    return this;
};
Counter.prototype.decrement = function () {
    this.value -= this.step;
};

let count1 = new Counter(3);
let count2 = new Counter(4);

const counter = new Counter(2);
counter.increment().increment().increment().value;
console.log(count1, count2, counter);