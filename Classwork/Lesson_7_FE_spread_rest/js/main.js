// const block = document.createElement("table");
// let tr = block.appendChild(document.createElement("tr"));
// let th = tr.appendChild(document.createElement("th"));

// const grid = document.querySelector('#root');
// const player = document.createElement('div');
// player.className = 'player';
// let playerPosition = 0;
//
// let isGameStarted = false;
//
// for (let i = 0; i < 25; i++) {
//     const cell = document.createElement('div');
//     cell.className = 'cell';
//     grid.appendChild(cell);
// }
//
// grid.addEventListener('click', function (evt) {
//     if(evt.target.classList.contains('cell')) {
//         const cell = evt.target;
//         const cellIndex = cell.dataset.index;
//         if (!isGameStarted) {
//             cell.appendChild(player);
//             playerPosition = cellIndex;
//             isGameStarted = true;
//         } else {
//              const shifts = [-6,-5,-4,-1,1,4,5,6];
//              for (let shift of shifts) {
//                  if (+cellIndex + shift == playerPosition){
//                      cell.appendChild(player);
//                      playerPosition = cellIndex;
//                  }
//              }
//         }
//     }
// });

let user = { name: "John", years: 30 };

const {name, years: age, isAdmin = false} = user;

console.log(user);
console.log(name);
console.log(age);
console.log(isAdmin);

const  arr = [1,2,3,4,5];

const [first, ...rest] = arr;

const allData = {age: 20, isAdmin: false, ...data};

