// function Person(name = 'John', surname = 'Doe') {
//    this.name = name;
//    this.surname = surname;
// }
//
// Person.prototype.getFullName = function () {
//    return `${this.name} ${this.surname}`
// };
//
// Person.prototype.changeFullName = function (fullname) {
//     const arr = fullname.split(' ');
//     if (arr.length !== 2) {
//         console.log('Err. Data is not valid.')
//     } else {
//         const [
//             name,
//             surname
//         ] = arr;
//         this.name = name;
//         this.surname = surname;
//     }
// };
// const first = new Person();
// const second = new Person('Sara', 'Connor');
// second.getFullName();
//
// first.changeFullName('Sara Connor');
//
// console.log(second.getFullName());
// console.log(second.changeFullName());

// const body = document.body;
//
// const block = document.createElement('div');
// body.appendChild(block);
//
// const list = block.appendChild(document.createElement('ul'));

//
// List.prototype.removeNote = function (num) {
//
// };
//
// console.log(first);
// console.log(first.addNote);


// List.prototype.addNote() =
// List.prototype.removeNote() =

// ==== Task 2 from teacher ====

// function List(parentEl) {
//     this.notes = [];
//     this.listEl = document.createElement('ol');
//     parentEl.appendChild(this.listEl);
// }
//
// const firstList = new List(document.body);
//
// List.prototype.addNote = function (note) {
//     this.notes.push(note);
//
//     const listItemEl = document.createElement('li');
//     listItemEl.textContent = note;
//     this.listEl.appendChild(listItemEl);
// };
//
// List.prototype.removeNote = function (index) {
//     this.notes.splice(index - 1, 1);
//
//     this.listEl.children[index - 1].remove();
// };

// ==== Task 3 ==========================================

function Form(parentEl, fields = {}, onSubmit = () => {}) {
    // this.fields = []; // Як сам робив
    this.formEl = document.createElement('form');
    this.fields = {};

    this.submitBtn = document.createElement('button');
    this.submitBtn.type = 'submit';
    this.submitBtn.innerText = 'SEND';
    this.formEl.appendChild(this.submitBtn);

    for(let name in fields) {
        this.addField(name);
    }

    this.formEl.addEventListener('submit', (e) => {
       e.preventDefault();
       const values = {};
       for (let name in this.fields) {
          values[name] = this.formEl.elements[name].value
       }
       onSubmit(values);
    });

    parentEl.appendChild(this.formEl);
    // this.field = {email: '', password: ''}; // Як сам робив
}

Form.prototype.addField = function (name) {
    if (!(name in this.fields)) {
        let field = document.createElement('input');
        field.name = name;
        field.type = 'text';
        this.formEl.insertBefore(field, this.submitBtn);
        this.fields[name] = '';
    }
};

Form.prototype.removeField = function (name) {
    if ((name in this.fields)) {
        let field = this.formEl.querySelector(`[name="${name}"]`);
        field.remove();
        delete this.fields[name];
    }
};

const firstForm = new Form(
    document.body,
    {
    email: '',
    password: ''
    },
    (values) => console.log(values)
);

firstForm.addField('name');
firstForm.addField('email');


console.log(firstForm);


