//
// let heading = '';
//
// const promise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         console.log('resolve');
//         resolve('Chain complete!');
//         heading = 'New value';
//
//     }, 2000);
//     // setTimeout(() => {
//     //     reject('Hello World!');
//     // }, 1000);
//
// });
// promise
//     .then((text) => {
//        return new Promise((res) => setTimeout(() => res(text + '!!!'), 2000))
//     })
//     .then(text => console.log(text))
//     .catch((err) => console.log(err))

// Task 1
// function slowForm() {
//     return new Promise((res, rej) => {
//         setTimeout(() => {
//             let age = +prompt("Enter your age");
//             if (age < 0 && age > 120 && !isNaN(age) && age !== null) {
//                 return rej(console.log('Sorry enter correct age'));
//             } else {
//                 res(+age);
//             }
//         }, 2000)
//     });
// }
//
// slowForm()
//     .then((age) => console.log(`User age is ${age}`))
//     .catch((err) => console.log(err.message));
//
// // slowForm.then();
// Ajax ==========================================
// const xhr = new XMLHttpRequest();
// // xhr.onreadystatechange = () => {
// //     console.log(xhr.readyState, xhr.response)
// // };
// xhr.open('GET', 'https://swapi.dev/api/people/1/');
// xhr.send();
//
// xhr.onload = () => {
//     // console.log(xhr.response)
// };

// function getData(url) {
//    return new Promise(((resolve) => {
//        console.log('Preparing');
//        setTimeout(() => {
//            const xhr = new XMLHttpRequest();
//            xhr.open('GET', url);
//            xhr.send();
//            xhr.onload = () => {
//                resolve(JSON.parse(xhr.response))
//            };
//            console.log('Done');
//
//
//        }, 2000)
//    }))
// }
//
// getData('https://swapi.dev/api/people/1/')
//     .then(data => console.log('Name is ' + data.name));
//
//
// // xhr.onprogress = (evt) => console.log(evt);

// fetch ==========================================
// Task 1 ============
const btn = document.createElement('button');
btn.textContent = 'GET LIST';
document.body.appendChild(btn);
const list = document.createElement('ul');
document.body.appendChild(list);

btn.addEventListener('click', function (e) {

    btn.style.cssText = "display: none";
    getList();
    loadMore();
});

const loadMoreBtn = document.createElement('button');
let pages = 0;



loadMoreBtn.addEventListener('click', function () {
   getList();
});

const getList = () => {
    pages++;
    if (pages === 9) {
        loadMoreBtn.style.cssText = 'display: none';
    }
    fetch(`https://swapi.dev/api/people/?page=${pages}`)
        .then(response => response.json())
        .then(data => {
            let listOfNames = data.results;
            listOfNames.forEach((e) => {

                const {name, ...arg} = e;

                let item = document.createElement('li');
                item.textContent = name;
                list.appendChild(item);

            });
            console.log(listOfNames)
        })
};

function loadMore() {
    loadMoreBtn.textContent = 'LOAD MORE';
    document.body.prepend(loadMoreBtn)
}


// fetch('http://swapi.dev/api/films/1')
//     .then(response => response.json())
//     .then(data => console.log(data))
