class Machine {

    static TURN_ON_TIME = 2000;

    constructor(name, date = new Date(), guaranteeTerm = null) {
        this.name = name;
        this.date = date;
        this.guaranteeTerm = guaranteeTerm;
    }

    turnOn() {
        console.log(`'${this.name}' - is loading...`);
        setTimeout(() => console.log("Hi! I`m turned on."), Machine.TURN_ON_TIME)
    }
}

class Supplies {
    constructor(coffee, water, milk, sugar) {
        this.coffee = coffee;
        this.water = water;
        this.milk = milk;
        this.sugar = sugar;
    }

}

class Drink {
    constructor(name, price, supplies) {
        const {coffee, water, milk, sugar} = supplies;

        this.name = name;
        this.price = price;
        this.supplies = supplies;
        // this.coffee = coffee;
        // this.water = water;
        // this.milk = milk;
        // this.sugar = sugar;
    }

}

class CoffeeMachine extends Machine {

    elements = {
        container: document.createElement('div'),
        turnOnBtn: document.createElement('button'),
        orderBtn: document.createElement('button'),
    };

    // drinks = [];
    // supplies = [];
    constructor(name, date, guaranteeTerm, supplies, drinks) {
        super(name, date, guaranteeTerm);
        this.supplies = supplies;
        this.drinks = drinks;
    }


    render() {
        const {container, turnOnBtn} = this.elements;
        turnOnBtn.classList.add('coffee__btn');
        turnOnBtn.addEventListener('click', (e) => this.handleTurnOn(e));
        turnOnBtn.textContent = "Turn on Coffee Machine";

        container.prepend(turnOnBtn);

        document.body.prepend(container);
    }

    turnOn() {
        super.turnOn();
        setTimeout(() => console.log("Wanna drink something?"), Machine.TURN_ON_TIME)
    }

    handleTurnOn(event) {
        const {container, turnOnBtn, orderBtn} = this.elements;
        orderBtn.textContent = "Make order";
        orderBtn.classList.add('coffee__btn');

        const drinksList = document.createElement('ul');
        drinksList.classList.add("drinks");


        const drinksItems = this.drinks.map(drink => {
            const drinkElement = document.createElement('li');
            drinkElement.classList.add("drinks__item");

            drinkElement.textContent = `${drink.name} - ${drink.price}`;
            console.log(drinkElement);

            return drinkElement;
        });

        orderBtn.addEventListener('click', (e) => this.handleMakeOrder(e));

        turnOnBtn.remove();
        drinksList.append(...drinksItems);
        container.append(orderBtn, drinksList);

        console.log(drinksItems);
        console.log(drinksList);
    }

    handleMakeOrder() {
        const {container, orderBtn} = this.elements;
        const orderForm = document.createElement('form');
        const orderName = document.createElement('input');
        const orderSubmit = document.createElement('input');
        orderSubmit.type = 'submit';
        orderSubmit.value = "Submit";


        orderForm.addEventListener('submit', (e) => this.handleOrderSubmit(e, orderName));

        orderForm.append(orderName, orderSubmit);

        orderBtn.remove();
        container.prepend(orderForm);
    }

    handleOrderSubmit(event, nameInput) {
        event.preventDefault();

        if (this.isDrinkExist(nameInput.value) && this.isEnoughSupplies(nameInput.value)) {
            setTimeout(() => {
                //    Throw success message
                console.log(`You ordered ${nameInput.value}. Here it is`);
            }, Machine.TURN_ON_TIME);
        } else {
            //    throw error message
            console.log(`Something went wrong`);
        }
    }

    isDrinkExist(drinkName) {
        return this.drinks.find(drink => drink.name === drinkName);
    }

    isEnoughSupplies(drinkName) {
        const userChoice = this.isDrinkExist(drinkName);
        let flag = true;

        Object.keys(this.supplies).forEach((supplyName) => {
            if (this.supplies[supplyName] < userChoice.supplies[supplyName]) {
                flag = false;
            }
        });

        return flag;
    }

}


const coffeeMachine = new CoffeeMachine(
    "Coffee dealer 2.0",
    new Date('10/1/1985'),
    2,
    new Supplies(10, 10, 10, 10),
    [
        new Drink("Latte", 32, new Supplies(1, 0, 3, 1)),
        new Drink("Espresso", 15, new Supplies(3, 0, 0, 0)),
        new Drink("Cappuccino", 35, new Supplies(2, 0, 2, 1)),
        new Drink("Americano", 25, new Supplies(2, 2, 0, 2))
    ]
);

// coffeeMachine.turnOn();
coffeeMachine.render();

console.log(coffeeMachine);

// const machine = new Machine('human energizer 6444');
// // const machine2 = new Machine('worst coffee I ever had');
//
// // machine2.turnOn();
// machine.turnOn();
//
// console.log(machine);