

const listOfFilms = document.createElement('ul');
const body = document.body;
body.appendChild(listOfFilms);

fetch('https://swapi.dev/api/films/')
    .then(response => response.json())
    .then(data => {
        let listOfNames = data.results;
        console.log(listOfNames);
        listOfNames.forEach((e) => {

            const {title, episode_id, opening_crawl, characters, ...arg} = e;
            console.log(characters);
            console.log(`Film name is "${title}"`, `Episode #${episode_id}`, `Info about film ${opening_crawl}`);
            let item = document.createElement('li');
            item.textContent = title;
            listOfFilms.appendChild(item);

        });
    });

console.log(listOfFilms);