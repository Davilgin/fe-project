class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(name) {
        // if need some checkout;
        this._name = name;
        return this._name;
    }

    set age(years) {
        // if need some checkout;
        this._age = years;
        return this._age;
    }

    set salary(value) {
        // if need some checkout;
        this._salary = value;
        return this._salary;
    }
}

class Programmer extends Employee {
     constructor(name, age, salary, lang = []) {
         super(name, age, salary);
         this.lang = lang;
     }

    set salary(value) {
        // if need some checkout;
        this._salary = value * 3;
        return this._salary;
    }

    get lang() {
        return this._lang;
    }

    set lang(lang) {
        // if need some checkout;
        this._lang = lang;
        return this._lang;
    }

}

const frontend = new Programmer('Anna', 21, 15000, ['JS']);
const backend = new Programmer('Mariia', 18, 21000, ['JS', 'PHP']);
const fullstack = new Programmer('Davyd', 22, 37000, ['JS', 'PHP', 'Ruby']);

console.log(frontend);
console.log(backend);
console.log(fullstack);